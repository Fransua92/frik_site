<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<section id="primary" class="content-area col-sm-12 col-lg-8">
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post();

				$post_link = get_post_permalink( $post->ID );?>
				<?php $post_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
					if (has_post_thumbnail( $post->ID ) ): ?>
						<a href="<?php echo $post_link?>">
						<img src="<?php echo $post_image[0]; ?>"></a>
				 <?php endif;?>
			 <a href="<?php echo $post_link?>">

			 <h2><?php echo get_the_title( $post->ID ); ?> </h2></a><?php
			 echo get_the_content();
			get_template_part( 'template-parts/content', get_post_format() );

			    the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_sidebar();
get_footer();
