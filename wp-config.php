<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'frik_test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Te^|.H{O.0Jtr&:}]Mj&l/2.[c{%[]6BpC}-qqSG6nfLzmQq$,.@P$}uh-e9gb*5');
define('SECURE_AUTH_KEY',  'AH4I/[,#qpt}6-Lw:-.gy{n(Q*nmvVYQ+[<_G[`nz,+@Fr|V@Lurgc*PEzI%XMVH');
define('LOGGED_IN_KEY',    'HXO;x}133e8#K]g%${-11Er}c6z~8$w,_j^Mq]|u1g(AnGO186:[Hfe6DCK`!RR ');
define('NONCE_KEY',        'o >9_CUZG8qJ>2poDgAAjE$5d#*b^/c$xg65uS4a_e:Kv3kXDm=NPebU@+~e+T]V');
define('AUTH_SALT',        '2xNyaSp .X6wz#Rty>j(n%8]]1]FHNe4&~dCxOqs}8E?U:3Cet1B#C{a{{3t`9en');
define('SECURE_AUTH_SALT', 'B&!(F.)Cd`,HmjF_qjoIpyc3c9/l;l(@5t*;fWt*=4YyP<MeDgC)!~}{YQ&!WUG;');
define('LOGGED_IN_SALT',   '0UqMJ51{.-.<`U>L :2t$21H<n^Pyb.bUhT0ZjnKZj,$~p.eLKHO@e%PL1uK41{d');
define('NONCE_SALT',       'f,vI{1gKw=pBj;-*16ka&2Plu]C;Ra}L+RmkQ+U;Pj1P@3f6cC)vV)es07_Z7?%t');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
